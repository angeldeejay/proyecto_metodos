from PySide2.QtWidgets import QTableWidgetItem, QAbstractScrollArea
from scipy._lib.six import xrange


def fillRombergResults(table, start, end, math_results, result):
    table.setColumnCount(len(math_results[-1]) + 2)
    table.setEnabled(True)
    table.setHorizontalHeaderItem(0, QTableWidgetItem("Subintervalos"))
    table.setHorizontalHeaderItem(1, QTableWidgetItem("Tamaño"))
    table.horizontalHeader().setVisible(True)
    table.setRowCount(len(math_results))

    # Data
    interval_start = int(start)
    interval_end = int(end)
    interval = range(interval_start, interval_end)
    interval_size = interval_end - interval_start

    i = j = 0
    for i in xrange(len(math_results)):
        table.setHorizontalHeaderItem(i + 2, QTableWidgetItem("T" + str(i)))
    for i in xrange(len(math_results)):
        table.setItem(i, 0, QTableWidgetItem(str(2**i)))
        table.setItem(i, 1, QTableWidgetItem(str(interval_size/(2.**i))))
        for j in xrange(i+1):
            table.setItem(i, j + 2, QTableWidgetItem(str(math_results[i][j])))

    table.setSizeAdjustPolicy(QAbstractScrollArea.AdjustToContents)
    table.resizeColumnsToContents()


def fillBisectionResults(table, e, math_results):
    table.setColumnCount(len(math_results[0]))
    table.setEnabled(True)
    table.setHorizontalHeaderItem(0, QTableWidgetItem("Iteración"))
    table.setHorizontalHeaderItem(1, QTableWidgetItem("a"))
    table.setHorizontalHeaderItem(2, QTableWidgetItem("b"))
    table.setHorizontalHeaderItem(3, QTableWidgetItem("Pm"))
    table.setHorizontalHeaderItem(4, QTableWidgetItem("Fa"))
    table.setHorizontalHeaderItem(5, QTableWidgetItem("Fb"))
    table.setHorizontalHeaderItem(6, QTableWidgetItem("FPm"))
    table.setHorizontalHeaderItem(7, QTableWidgetItem("Signo"))
    table.horizontalHeader().setVisible(True)
    table.setRowCount(len(math_results))

    # Data
    for i in xrange(len(math_results)):
        for j in xrange(len(math_results[i])):
            table.setItem(i, j, QTableWidgetItem(str(math_results[i][j])))

    table.setSizeAdjustPolicy(QAbstractScrollArea.AdjustToContents)
    table.resizeColumnsToContents()


def fillFixedPointResults(table, e, math_results):
    table.setColumnCount(len(math_results[0]))
    table.setEnabled(True)
    table.setHorizontalHeaderItem(0, QTableWidgetItem("Iteración"))
    table.setHorizontalHeaderItem(1, QTableWidgetItem("x"))
    table.setHorizontalHeaderItem(2, QTableWidgetItem("Fx"))
    table.setHorizontalHeaderItem(3, QTableWidgetItem("e"))
    table.horizontalHeader().setVisible(True)
    table.setRowCount(len(math_results))

    # Data
    for i in xrange(len(math_results)):
        for j in xrange(len(math_results[i])):
            value = int(math_results[i][j]) if j == 0 else math_results[i][j]
            table.setItem(i, j, QTableWidgetItem(str(value)))

    table.setSizeAdjustPolicy(QAbstractScrollArea.AdjustToContents)
    table.resizeColumnsToContents()
