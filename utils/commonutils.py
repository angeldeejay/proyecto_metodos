def trim(text):
    return text.rstrip('\n\t\s ').lstrip('\n\t\s ')
