import numpy as np
import inspect
import math
from scipy._lib.six import xrange


class Romberg:
    # default constructor
    def __init__(self, function_return="x", interval_start=0, interval_end=1,
                 tolerance=1.0e-10):
        self.function_return = function_return
        self.function = lambda x: eval(function_return)
        self.interval_start = interval_start
        self.interval_end = interval_end
        self.interval = [interval_start, interval_end]
        self.tolerance = tolerance

    def _difftrap(self, num_traps=1):
        if num_traps <= 0:
            raise ValueError("num_traps debe ser > 0")
        elif num_traps == 1:
            return 0.5*(self.function(self.interval[0])+self.function(self.interval[1]))
        else:
            num_to_sum = num_traps/2
            h = float(self.interval[1]-self.interval[0])/num_to_sum
            lox = self.interval[0] + 0.5 * h
            points = lox + h * np.arange(num_to_sum)
            s = np.sum(self.function(points), axis=0)
            return s

    def _vectorize(self, args=()):
        def vector_function(x):
            if np.isscalar(x):
                return self.function(x, *args)
            x = np.asarray(x)
            # call with first point to get output type
            y0 = self.function(x[0], *args)
            n = len(x)
            dtype = getattr(y0, 'dtype', type(y0))
            output = np.empty((n,), dtype=dtype)
            output[0] = y0
            for i in xrange(1, n):
                output[i] = self.function(x[i], *args)
            return output
        return vector_function

    def _romberg_diff(self, interval_end, c, k):
        tmp = 4.0**k
        return (tmp * c - interval_end)/(tmp - 1.0)

    def integrate(self, deep=10):
        if np.isinf(self.interval_start) or np.isinf(self.interval_end):
            raise ValueError(
                "Este método numérico es válido solo para intervalos finitos")
        vector_function = self._vectorize()
        n = 1
        range_delta = self.interval_end - self.interval_start
        ordinal_sum = self._difftrap(num_traps=n)
        result = range_delta * ordinal_sum
        math_results = [[result]]
        error = np.inf
        last_row = math_results[0]
        for i in xrange(1, deep+1):
            n *= 2
            ordinal_sum += self._difftrap(num_traps=n)
            row = [range_delta * ordinal_sum / n]
            for k in xrange(i):
                row.append(self._romberg_diff(last_row[k], row[k], k+1))
            result = row[i]
            last_result = last_row[i-1]
            math_results.append(row)
            error = abs(result - last_result)
            if error < self.tolerance or error < self.tolerance * abs(result):
                break
            last_row = row
        else:
            print("No se encontró resultado con límite de %d subintervalos" %
                  deep, end='. ')
            print('Última diferencia calculada: %e' % error)

        return result, math_results
