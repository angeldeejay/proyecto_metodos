from pylab import plot, show
import numpy as np


class Bisection:
    def __init__(self, function_return="x"):
        self.function_return = function_return
        self.function = lambda x: eval(function_return)

    def solve(self, interval_start=0, interval_end=1, iterations=100):
        a = interval_start
        b = interval_end
        e = 1
        i = 0
        matrix = np.empty((0, 8))

        while (i <= iterations):
            p = (b + a) / 2
            result_a = self.function(a)
            result_b = self.function(b)
            result_p = self.function(p)
            sign = '+' if result_a * result_p > 0 else '-'
            matrix = np.append(matrix, np.array(
                [[i, a, b, p, result_a, result_b, result_p, sign]]), axis=0)
            if sign == '+':
                a = p
            else:
                b = p
            e = result_p
            i = i + 1

        return e, matrix
