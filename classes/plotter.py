import numpy as np
import matplotlib.pyplot as plt
from sympy import latex, sympify
from matplotlib.patches import Polygon


class Plotter:
    def function_to_latex(function_return):
        return latex(sympify(function_return))

    def plot_integral(function_return, interval_start, interval_end):
        def function(x): return eval(function_return)
        range_delta = abs(interval_end - interval_start)
        series_data = [[], []]
        for x in np.arange(interval_start, interval_end, 0.01):
            series_data[0].append(x)
            series_data[1].append(function(x))

        figure, axis = plt.subplots(1)
        x = np.linspace(interval_start - 1, interval_end + 1)
        axis.plot(x, function(x), 'r', linewidth=2,
                  scalex=True, scaley=True)
        axis.axhline(y=0, color='b', linewidth=1.25)
        axis.axvline(x=0, color='b', linewidth=1.25)

        # Make the shaded region
        ix = np.linspace(interval_start, interval_end)
        iy = function(ix)
        vertices = [(interval_start, 0), *zip(ix, iy), (interval_end, 0)]
        area = Polygon(vertices, facecolor='r', alpha=0.2, linewidth=1)
        axis.add_patch(area)
        axis.add_patch(area)

        function_latex = Plotter.function_to_latex(function_return)
        axis.text(interval_start + (range_delta / 2), 0.5,
                  '$\\int_' + str(interval_start) + '^' + str(interval_end) +
                  '{' + function_latex + '\\mathrm{d}x}$',
                  horizontalalignment='center')

        figure.text(0.9, 0.1, '$x$')
        figure.text(0.1, 0.9, '$f(x)='+function_latex+'$', color='r')

        axis.spines['right'].set_visible(False)
        axis.spines['top'].set_visible(False)

        axis.xaxis.set_ticks_position('bottom')
        axis.yaxis.set_ticks_position('left')

        axis.axis('equal')
        axis.grid(True)
        manager = plt.get_current_fig_manager()
        # manager.full_screen_toggle()
        plt.show()
