import math
from pylab import plot, show
import numpy as np
from numpy.linalg import norm


class FixedPoint:
    def __init__(self, function_return="x"):
        self.function_return = function_return
        self.function = lambda x: eval(function_return)

    def solve(self, initial_value=0, iterations=100, tolerance=1.0e-10):
        e = None
        step = 0
        x = initial_value
        y = None
        matrix = np.empty((0, 4))
        while ((e is None or e >= tolerance) and step < iterations):
            y = self.function(x)
            e = norm(x - y)
            matrix = np.append(matrix, np.array([[step, x, y, e]]), axis=0)
            x = y
            step = step + 1
        return y, e, matrix
