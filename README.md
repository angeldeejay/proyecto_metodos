# Proyecto de curso

## Métodos numéricos

### Grupo 511

- Wilmer Andrés Vanegas Jiménez
- Bryan Perafan Martinez
- Diego Fernando Collazos Perafan
- Julián David Ortíz Idrobo

### Instalación

Correr el comando `pip install -r requirements` en la consola para instalar todas las dependencias.

### Desarrollo

Todos los archivos están hechos en python, excepto por `views/mainwindow.py` que es generado con el comando `pyside2-uic views/mainwindow.ui > views/mainwindow.py`. El archivo UI es un archivo de interfaces de Qt Creator 5.

### Ejecución

Correr el comando `python main.py`.

### Screenshots

![](screenshots/romberg.png "Algoritmo de Romberg")

![](screenshots/romberg_done.png "Algoritmo de Romberg (Ejecutado)")

![](screenshots/romberg_graph.png "Algoritmo de Romberg (Gráfica)")
