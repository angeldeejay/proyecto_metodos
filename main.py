# This Python file uses the following encoding: utf-8
import sys
from PySide2 import QtWidgets
from PySide2.QtWidgets import QApplication, QMainWindow, QTableWidgetItem
from utils.tableutils import fillRombergResults, fillBisectionResults, fillFixedPointResults
from utils.commonutils import trim
from views.mainwindow import Ui_MainWindow
from classes.romberg import Romberg
from classes.bisection import Bisection
from classes.fixed_point import FixedPoint
from classes.plotter import Plotter


class Main(QMainWindow, Ui_MainWindow):
    def __init__(self):
        QMainWindow.__init__(self)
        self.setupUi(self)

        self.rombergComputeButton.clicked.connect(self.computeRomberg)
        self.rombergPlotButton.clicked.connect(self.plotRomberg)
        self.bisectionComputeButton.clicked.connect(self.computeBisection)
        self.fixedPointComputeButton.clicked.connect(self.computeFixedPoint)

    def computeFixedPoint(self):
        calculator = FixedPoint(function_return=trim(
            self.fixedPointLambdaReturn.text()))
        result, e, fixedPointMatrix = calculator.solve(initial_value=float(self.fixedPointInitialValue.text().replace(',', '.')),
                                                       tolerance=eval(
                                                           str(self.fixedPointTolerance.value())),
                                                       iterations=int(self.fixedPointIterations.value()))

        fillFixedPointResults(self.fixedPointResultsTable, e, fixedPointMatrix)
        self.fixedPointResultsLabel.setText(
            "La solución es " + str(result) + " con un error de " + str(e))

    def computeBisection(self):
        calculator = Bisection(function_return=trim(
            self.bisectionLambdaReturn.text()))
        e, bisectionMatrix = calculator.solve(interval_start=int(self.bisectionIntervalStart.value()),
                                              interval_end=int(
                                                  self.bisectionIntervalEnd.value()),
                                              iterations=int(self.bisectionIterations.value()))

        fillBisectionResults(self.bisectionResultsTable, e, bisectionMatrix)
        self.bisectionResultsLabel.setText("La solución es " + str(
            bisectionMatrix[-1][3]) + " con un error de " + str(bisectionMatrix[-1][6]))

    def computeRomberg(self):
        calculator = Romberg(function_return=trim(self.rombergLambdaReturn.text()),
                             interval_start=int(
                                 self.rombergIntervalStart.value()),
                             interval_end=int(self.rombergIntervalEnd.value()))

        rombergResult, rombergMatrix = calculator.integrate()

        fillRombergResults(self.rombergResultsTable,
                           self.rombergIntervalStart.value(), self.rombergIntervalEnd.value(),
                           rombergMatrix, rombergResult)

        self.rombergPlotButton.setEnabled(True)

    def plotRomberg(self):
        self.rombergPlotButton.setEnabled(False)

        Plotter.plot_integral(trim(self.rombergLambdaReturn.text()),
                              int(self.rombergIntervalStart.value()),
                              int(self.rombergIntervalEnd.value()))


if __name__ == "__main__":
    app = QApplication([])
    window = Main()
    window.show()
    sys.exit(app.exec_())
